const got = require("../data"); //importing object
function nameWithA(got) {
  //creating function
  try {
    let result; //created variable
    Object.entries(got).forEach(([keys, allData]) => {
      //stored value in array using entries and than iterated using reduce
      const allNamesWithA = allData.reduce((nameWithA, value) => {
        //again iterated inner array using reduce
        if (value.hasOwnProperty("people")) {
          value["people"].forEach((element) => {
            //iterating inside people
            if (element.name.includes("A") || element.name.includes("a")) {
              nameWithA.push(element.name); //if condition satisifies pushing value inside array
            }
          });
        }
        return nameWithA; //returning cb
      }, []);
      result = allNamesWithA;
    });
    return result; //returning output
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
const result = nameWithA(got);
console.log(result);
