const got = require("../data"); //importing object
function everyone(got) {
  //creating function
  try {
    const namesOfAll = Object.entries(got).reduce(
      //stored value in array using entries and than iterated using reduce
      (allNames, [keys, allData]) => {
        const returnedNames = allData.reduce((names, values) => {
          //again iterated inner array using reduce
          if (values.hasOwnProperty("people")) {
            values["people"].forEach((element) => {
              //iterated using forEach
              names.push(element.name); //pushing names in cb array
            });
          }
          return names; //returned cb
        }, []);
        allNames = returnedNames;
        return allNames; //returning array
      },
      []
    );
    return namesOfAll; //returning output
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
const result = everyone(got);
console.log(result);
