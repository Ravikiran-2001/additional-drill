const got = require("../data"); //importing object
function peopleNameOfAllHouses(got) {
  //creating function
  try {
    const result = Object.entries(got).reduce(
      //stored value in array using entries and than iterated using reduce
      (allNamesPerClan, [, allData]) => {
        allData.forEach((clan) => {
          if (allNamesPerClan.hasOwnProperty(clan.name) === false) {
            allNamesPerClan[clan.name] = []; //if property does'nt exist created a property
          }
          clan.people.forEach((details) => {
            //iterating inside people array
            allNamesPerClan[clan.name].push(details.name); //pushing names respective of there clan
          });
        });
        return allNamesPerClan; //returned cb
      },
      {}
    );
    return result; //returning output
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
console.log(peopleNameOfAllHouses(got));
