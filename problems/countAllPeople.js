const got = require("../data"); //importing object
function countAllPeople(got) {
  //creating function
  try {
    //implementation written under try block
    const peopleCount = Object.entries(got).map(([key, alldata]) => {
      //stored data of object inside array and used map method on array
      return alldata.reduce((count, houses) => {
        //using reduce method to iterate and cb variable to count number of names
        if (houses.hasOwnProperty("people")) {
          //if has property
          count += houses.people.length; //counting
        }
        return count; //returning cb
      }, 0);
    });
    let totalPeopleCount = peopleCount.reduce(
      //storing count value inside different variable by using reduce method
      (totalCount, count) => totalCount + count,
      0
    );
    return totalPeopleCount; //returning variable
  } catch (error) {
    console.error("An error occurred:", error.message); //if error occurs than this will be printed
  }
}
console.log(countAllPeople(got)); //calling and printing returned value
