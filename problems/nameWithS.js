const got = require("../data"); //importing object
function nameWithS(got) {
  //creating function
  try {
    let result; //created variable
    Object.entries(got).forEach(([keys, allData]) => {
      //stored value in array using entries and than iterated using reduce
      const allNamesWithS = allData.reduce((nameWithS, value) => {
        //again iterated inner array using reduce
        if (value.hasOwnProperty("people")) {
          value["people"].forEach((element) => {
            //iterating inside people
            if (element.name.includes("S") || element.name.includes("s")) {
              nameWithS.push(element.name); //if condition satisifies pushing value inside array
            }
          });
        }
        return nameWithS; //returning cb
      }, []);
      result = allNamesWithS;
    });
    return result; //returning output
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
const result = nameWithS(got);
console.log(result);
