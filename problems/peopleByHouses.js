const got = require("../data"); //importing object
function peopleByHouses(got) {
  //creating function
  try {
    const numberOfPeopleByHouse = Object.entries(got).reduce(
      //stored value in array using entries and than iterated using reduce
      (totalPeopleByHouse, [houses, allData]) => {
        const totalPeople = allData.reduce((people_Houses, clans) => {
          if (clans.hasOwnProperty("people")) {
            people_Houses[clans.name] = clans.people.length; //if have property adding value
          }
          return people_Houses; //returning cb
        }, {});
        totalPeopleByHouse = totalPeople; //asigning value
        return totalPeopleByHouse; //returning cb
      },
      {}
    );
    return numberOfPeopleByHouse; //returning output
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
const result = peopleByHouses(got);
console.log(result);
