const got = require("../data"); //importing object
function surnameWithS(got) {
  //creating function
  try {
    let resutNames = []; //created empty array
    Object.entries(got).forEach(([houses, allData]) => {
      //stored value in array using entries and than iterated using reduce
      allData.forEach((house) => {
        //iterating inside array
        if (house.hasOwnProperty("people")) {
          house["people"].forEach((value) => {
            //iterating inside people property
            const name = value.name.split(" "); //splitting string
            let letter = name[name.length - 1].slice(0, 1); //taking first letter of last word and comparing
            if (letter == "S") {
              resutNames.push(value.name); //if true pushed
            }
          });
        }
      });
    });
    return resutNames; //retuning output
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
console.log(surnameWithS(got));
